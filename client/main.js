
import Phaser from "phaser";
window.Phaser = Phaser
window._ = require('lodash')
//set some globals for ease of use

var IO = require('socket.io-client')
var io = new IO()
io.on('connect', function() {
    console.log('socketio connected')
})
io.on('disconnect', function() {
    setTimeout(function() {
        window.location.reload(true)

    }, 1500)
})

var w = window.innerWidth
var h = window.innerHeight
window.w = w
window.h = h
var config = {
    type: Phaser.AUTO,
    width: w,
    height: h,
    transparent: true,
    antialias: true,
    parent: 'scene',
    scene: [
        require('./scenes/play'),
    ]
}

window.game = new Phaser.Game(config)