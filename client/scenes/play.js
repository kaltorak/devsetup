let shortid = require('shortid')
module.exports = new Phaser.Class({
    Extends: Phaser.Scene,
    initialize: function play() {
        Phaser.Scene.call(this, {
            key: 'play'
            //plugins: []
        })
    },

    preload: function() {
        this.load.image('rune', '/img/rune.png')
    },

    create: function() {
        this.rune = this.add.sprite(100, 100, 'rune')
    },

    update: function(time, delta) {
        this.rune.setRotation(this.rune.rotation + delta * .001)
    }
})